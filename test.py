import openpyxl
import sys

# Getting File Name from Shell script
fileName = sys.argv[1]

# Define variables to load the dataframe
dataframe = openpyxl.load_workbook("Book1.xlsx")

# Define variables to read sheet
dataframe1 = dataframe.active

#Text file object
textobj = open(fileName, 'w')

# Iterate the loop to read the cell values
for row in range(0, dataframe1.max_row):
    for col in dataframe1.iter_cols(1, dataframe1.max_column):
          print(col[row].value)
          textobj.writelines(str(col[row].value) + "     ")
    textobj.writelines("\n")
textobj.close
