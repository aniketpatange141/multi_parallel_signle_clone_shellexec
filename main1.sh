#!/bin/bash

ROOT_DIR=${PWD}
echo ${ROOT_DIR}

if [[ -d ${ROOT_DIR}/artifacts ]]; then
    sudo rm -rf ${ROOT_DIR}/artifacts
fi

mkdir -p ${ROOT_DIR}/artifacts/txt
chmod 777 artifacts
chmod 777 artifacts/txt
ls
#add python script calling part    1. edit the name of python file 2. Pass the required arguments using ${name_of_the_arguments} ex ${ROOT_DIR} python ${ROOT_DIR}/test.python
#sudo touch Book1.txt
#sudo chmod 777 Book1.txt
python3 ${ROOT_DIR}/test.py ${ROOT_DIR}/"Book1.txt"
# edit path of the txt file
cd ${ROOT_DIR}

cp -f Book1.txt ${ROOT_DIR}/artifacts/txt/
